---
title: WritingPlan
date: 2019-06-13 17:36:38
tags:
---

* UE4

    * Rendering Pipline
    * Custom Rendering Path

* PlayCanvas
* Common Algorithm

    * 滤波
    * Ping-pong

![test](_v_images/20200421093739116_49.png)

* Gameplay Design

```puml
@startuml
class A
@enduml
```

$\sqrt{3x-1}+(1+x)^2$

$$\sqrt{4x+4}+(1+y)^2$$
