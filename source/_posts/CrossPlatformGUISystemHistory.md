# 跨平台GUI系统发展史

## wxWidgets

## Qt

## Java SWing

## Java SWT

## Adobe Flash

## Adobe AIR

## Autodesk ScaleForm

## ReactNative

## Electron

## Flutter

## 简要对比

|     库      | 灵活性 | 可嵌入 | 性能 | 易扩展性 | 类型 | 原生平台通信 | 动画 | 滤镜 |
| ----------- | ----- | ----- | --- | ------- | --- | ----------- | ---- | ---- |
| wxWidgets   | 低     | 否     | 中   | 难      | 原生 | 简单        |      |      |
| Qt          | 高     | 否     | 高   | 中      | 自画 | 简单        |      |      |
| Java SWing  | 低     | 否     | 低   | 难      | 自画 | 简单        |      |      |
| Java SWT    | 低     | 否     | 中   | 难      | 原生 | 简单        |      |      |
| Aodbe Flash | 高     | 否     | 高   | 易      | 自画 | 不支持      | 支持 | 支持 |
| Aodbe AIR   | 高     | 否     | 高   | 易      | 自画 | 复杂        | 支持 | 支持 |
| ScaleForm   | 高     | 是     | 高   | 易      | 自画 | 简单        | 支持 | 支持 |
| ReactNative | 低     | 否     | 中   | 易      | 原生 | 简单        |      |      |
| Electron    | 高     | 否     | 中   | 易      | WEB  | 复杂        |      |      |
| Flutter     | 高     | 是     | 高   | 易      | 自画 | 简单        |      |      |